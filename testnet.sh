#!/bin/bash
clear && echo -e "\e[31m
╔╦╗╔═╗╔═╗╔╦╗╔╗╔╔═╗╔╦╗
 ║ ║╣ ╚═╗ ║ ║║║║╣  ║ 
 ╩ ╚═╝╚═╝ ╩ ╝╚╝╚═╝ ╩ \e[0m"
 mainmenu() {
    echo
    echo " ✨ | 1.Shardeum"
    echo " 👾 | 2.Allora"
    echo " 🌬️  | 3.Aligned"
    echo " 🤖 | 4.GaiaNet"
    echo " 💤 | 5.Nubit"
    echo " 🛩️  | 6.Farcaster"
    echo " 🌶️  | 7.Elixir"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/shardeum/main.sh) ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/allora/main.sh) ;;
        3) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/aligned/main.sh) ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/gaianet/main.sh) ;;
        5) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/nubit/main.sh) ;;
        6) source <(curl -s https://gitlab.com/blockchain-projekts/mainnet/-/raw/main/farcaster/node.sh) ;;
        7) source <(curl -s https://gitlab.com/blockchain-projekts/testnet/-/raw/main/elixir/main.sh) ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/launcher.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac

}
mainmenu