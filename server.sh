#!/bin/bash
clear && echo -e "\e[31m
╔═╗╔═╗╦═╗╦  ╦╔═╗╦═╗  ╔═╗╔═╗╔╗╔╔═╗╦╔═╗       
╚═╗║╣ ╠╦╝╚╗╔╝║╣ ╠╦╝  ║  ║ ║║║║╠╣ ║║ ╦       
╚═╝╚═╝╩╚═ ╚╝ ╚═╝╩╚═  ╚═╝╚═╝╝╚╝╚  ╩╚═╝\e[0m"
mainmenu() {
    echo
    echo " 🧭 | 1.Server ip"
    echo " 📊 | 2.Server Monitor"
    echo " 🛠️  | 3.Setting Ports"
    echo " 🛠️  | 4.Setting Proxy"
    echo " 🔒 | 5.Setting VPN"
    echo " 🔧 | 6.Base setting"
    echo " ----------------"
    echo " 0.Back | 10.Exit"
    echo " ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) echo -e "\e[33mYour IP\e[0m" && echo -e "\e[33m--------------\e[0m" && curl https://ipinfo.io/ip && echo && echo -e "\e[33m-------------- \e[0m" && mainmenu ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server/sm.sh) ;;
        3) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server/ports.sh) ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server/dante_proxy.sh) ;;
        5) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server/vpn.sh) ;;
        6) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server/base_setting.sh) ;;
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/launcher.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}
mainmenu