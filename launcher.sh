#!/bin/bash
clear && echo -e "\e[34m
╦ ╦╦╔═╗╔═╗╦═╗╔╦╗  ╔╗╔╔═╗╔╦╗╔═╗
║║║║╔═╝╠═╣╠╦╝ ║║  ║║║║ ║ ║║║╣ 
╚╩╝╩╚═╝╩ ╩╩╚══╩╝  ╝╚╝╚═╝═╩╝╚═╝\e[0m"
mainmenu() {
    echo
    echo " 🚀 | 1.Nodes Mainnet "
    echo " 🛸 | 2.Nodes Testnet"
    echo " 🧸 | 3.Smart Contracts"
    echo " 🛠️  | 4.Server"
    echo " 📚 | 5.About"
    echo "  ----------------"
    echo "      10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        1) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/mainnet.sh) ;;
        2) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/testnet.sh) ;;
        3) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/smart_contracts.sh) ;;
        4) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;
        5) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/about.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}
mainmenu