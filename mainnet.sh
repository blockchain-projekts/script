#!/bin/bash
clear && echo -e "\e[32m
╔╦╗╔═╗╦╔╗╔╔╗╔╔═╗╔╦╗
║║║╠═╣║║║║║║║║╣  ║ 
╩ ╩╩ ╩╩╝╚╝╝╚╝╚═╝ ╩\e[0m" 
mainmenu() {
    echo
    #echo " 1. Nibiru"
    #echo " 2. Axiom"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
    
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/launcher.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo "Invalid request!" && mainmenu ;;
    esac
}
mainmenu