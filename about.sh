#!/bin/bash
clear && echo -e "\e[34m
╔═╗╔╗ ╔═╗╦ ╦╔╦╗
╠═╣╠╩╗║ ║║ ║ ║ 
╩ ╩╚═╝╚═╝╚═╝ ╩ \e[0m" && echo
mainmenu() {
echo -ne "\e[3;29mThis script is designed to help you install nodes, 
launch smart contracts, and configure your server without losing your sanity) \e[0m"
echo
echo -e "\e[4;34mhttps://t.me/paradox_club_live \e[0m"
echo
echo -e "\e[1;32mIf this script was helpful, you can support: \e[0m"
echo -e "\e[3;33m0x5d69c3eE41755ecDf18767b63033F4F2116BACad\e[0m"
echo
echo -e "\e[3;29mNote: This script is written for personal use and assumes installation on a clean server. \e[0m"
echo -e "\e[3;29mYou can review the code at: https://gitlab.com/citadel-s_club \e[0m"
echo
echo -ne "\e[3;29m To return to the main menu, press Enter \e[0m"
read -p " "
source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/launcher.sh)
}
mainmenu
