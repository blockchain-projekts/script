#!/bin/bash
    clear && echo -e "\e[31m
╔═╗╔═╗╔═╗╔╗╔  ╦  ╦╔═╗╔╗╔
║ ║╠═╝║╣ ║║║  ╚╗╔╝╠═╝║║║
╚═╝╩  ╚═╝╝╚╝   ╚╝ ╩  ╝╚╝\e[0m"
mainmenu() {
echo
echo "--------------------------------"
echo -ne "This script will let you set up your own VPN server in no more than a minute, 
even if you haven't used OpenVPN before. It has been designed to be as unobtrusive and universal as possible. " && echo
echo "--------------------------------"
echo -e "\e[34mDeveloper: \e[0m https://github.com/Nyr/openvpn-install"
echo -e "\e[32mConfiguration file is saved in your home directory. \e[0m"
echo "--------------------------------"
echo -e "\e[34mFor installation, press Enter, to return to the menu, enter 1: \e[0m "
read -r ans
case $ans in
    1)  source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;
    *) wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh && source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;
esac

}
mainmenu