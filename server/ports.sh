#!/bin/bash
    clear && echo -e "\e[33m
┌─┐┌─┐┌┬┐┌┬┐┬┌┐┌┌─┐  ┌─┐┌─┐┬─┐┌┬┐┌─┐        
└─┐├┤  │  │ │││││ ┬  ├─┘│ │├┬┘ │ └─┐        
└─┘└─┘ ┴  ┴ ┴┘└┘└─┘  ┴  └─┘┴└─ ┴ └─┘ \e[0m" 
mainmenu() {
	echo
 	echo " 🪬 | 1.View active listening ports"
	echo " 🟢 | 2.Open port (ufw)"
	echo " 🔴 | 3.Close port (ufw)"
	echo " 🔵 | 4.Check port status (ufw)"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
		
		1) if ! command -v netstat &> /dev/null
            then
                echo "netstat is not installed. Installing..."
                sudo apt-get update && sudo apt-get install -y net-tools
            fi
            echo "See open ports" && netstat -tupln | grep LISTEN && mainmenu
            ;;
		
		2) if ! command -v ufw &> /dev/null
            then
                echo "ufw is not installed. Installing..."
                sudo apt-get update && sudo apt-get install -y ufw
            fi
		echo -ne "Enter the port number to open: "
		read -r port
		sudo ufw allow $port
		echo "Port $port opened"
		mainmenu
		;;

		3) echo -ne "Enter the port number to close: "
		read -r port
		sudo ufw deny $port
		echo "Port $port closed"
		mainmenu
		;;

		4) echo "Port status" && sudo ufw status numbered && mainmenu ;;

		0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;
		

		10) echo $(printBCyan '"Bye bye."') && exit ;;

		*) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;
	esac
}
mainmenu