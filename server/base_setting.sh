#!/bin/bash

clear && echo -e "\e[31m
╔═╗╔═╗╔═╗╦ ╦╦═╗╦╔╦╗╦ ╦  ╔═╗╔═╗╔╗╔╔═╗╦╔═╗
╚═╗║╣ ║  ║ ║╠╦╝║ ║ ╚╦╝  ║  ║ ║║║║╠╣ ║║ ╦
╚═╝╚═╝╚═╝╚═╝╩╚═╩ ╩  ╩   ╚═╝╚═╝╝╚╝╚  ╩╚═╝  \e[0m"

# Цвета для вывода
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Предупреждение
echo -e "\e[31mВНИМАНИЕ: Этот скрипт предназначен для выполнения только на свежем сервере. Убедитесь, что у вас нет важных данных или настроек, которые могут быть изменены.\e[0m"
read -p "Вы уверены, что хотите продолжить? (yes/no): " confirm
if [ "$confirm" != "yes" ]; then
    echo -e "\e[31mОперация отменена пользователем.\e[0m"
    exit 1
fi

# Проверка дистрибутива
echo -e "${BLUE}Проверка дистрибутива...${NC}"
if [ -f /etc/os-release ]; then
    . /etc/os-release
    if [[ "$ID" != "ubuntu" && "$ID" != "debian" ]]; then
        echo -e "${RED}Этот скрипт предназначен для Ubuntu или Debian. Вы используете $NAME. Прекращение работы. Пожалуйста, не обижайтесь!${NC}"
        exit 1
    fi
else
    echo -e "${RED}Не удалось определить дистрибутив. Прекращение работы. Возможно, ваш сервер инкогнито!${NC}"
    exit 1
fi

echo -e "${GREEN}Вы используете $NAME. Отличный выбор! Продолжаем настройку.${NC}"

# Обновление системы
echo -e "${BLUE}Обновление системы до последних версий пакетов...${NC}"
sudo apt update && sudo apt upgrade -y
# Установка необходимых пакетов
echo -e "${BLUE}Установка дополнительных пакетов...${NC}"
sudo apt install console-cyrillic git wget screen net-tools nano bash-completion -y 

# Установка и настройка брандмауэра
echo -e "${BLUE}Установка и настройка UFW (Uncomplicated Firewall)...${NC}"
sudo apt install ufw -y
sudo ufw default deny incoming
sudo ufw default allow outgoing
echo -e "${GREEN}По умолчанию все входящие соединения запрещены, а исходящие разрешены. Как в клубе: вход только по приглашениям!${NC}"

# Запрос на изменение порта для SSH
read -p "Хотите изменить порт для SSH? (yes/no, по умолчанию no): " change_ssh_port
change_ssh_port=${change_ssh_port:-no}

if [ "$change_ssh_port" == "yes" ]; then
    read -p "Введите новый номер порта для SSH: " ssh_port
    sudo ufw allow $ssh_port
    echo -e "${YELLOW}Порт $ssh_port разрешен для SSH. Это позволит вам подключаться к серверу через SSH. Как VIP-гость!${NC}"
else
    ssh_port=22
    sudo ufw allow $ssh_port
    echo -e "${YELLOW}Используется стандартный порт 22 для SSH. Классика жанра!${NC}"
fi

# Включение UFW
echo -e "${BLUE}Включение UFW для применения правил брандмауэра...${NC}"
sudo ufw enable

# Генерация SSH-ключа
read -p "Хотите создать новый SSH-ключ? (yes/no, по умолчанию no): " generate_ssh_key
generate_ssh_key=${generate_ssh_key:-no}
if [ "$generate_ssh_key" = "yes" ]; then
    ssh-keygen -t rsa -b 2048
    echo -e "${GREEN}SSH-ключ успешно создан. Пожалуйста, скопируйте публичный ключ на сервер. Это как передать ключи от дома!${NC}"
    echo -e "${YELLOW}Для этого используйте команду: ssh-copy-id -i ~/.ssh/id_rsa.pub user@server${NC}"
else
    echo -e "${YELLOW}Создание SSH-ключа пропущено. Надеюсь, у вас уже есть запасной!${NC}"
fi

# Настройка SSH
echo -e "${BLUE}Настройка SSH для повышения безопасности...${NC}"
sudo sed -i "s/#Port 22/Port $ssh_port/" /etc/ssh/sshd_config

## Запрос на отключение аутентификации по паролю
read -p "Отключить аутентификацию по паролю для SSH? (yes/no, по умолчанию no): " disable_password_auth
disable_password_auth=${disable_password_auth:-no}

if [ "$disable_password_auth" = "yes" ]; then
    if [ "$VERSION_ID" = "24.04" ]; then
        sudo sed -i "s/#PasswordAuthentication yes/PasswordAuthentication no/" /etc/ssh/sshd_config.d/50-cloud-init.conf
    else
        sudo sed -i "s/#PasswordAuthentication yes/PasswordAuthentication no/" /etc/ssh/sshd_config
    fi
    echo -e "${GREEN}Аутентификация по паролю отключена. Теперь для входа будет использоваться только SSH-ключ. Без паролей, только ключи!${NC}"
else
    echo -e "${YELLOW}Аутентификация по паролю оставлена включенной. Это может быть менее безопасно. Как оставить дверь открытой!${NC}"
fi

sudo sed -i "s/#PubkeyAuthentication yes/PubkeyAuthentication yes/" /etc/ssh/sshd_config
sudo sed -i "s/#AuthorizedKeysFile/AuthorizedKeysFile/" /etc/ssh/sshd_config

# Запрос на отключение входа под пользователем root
read -p "Хотите отключить вход под пользователем root? (yes/no, по умолчанию no): " disable_root_login
disable_root_login=${disable_root_login:-no}

if [ "$disable_root_login" = "yes" ]; then
    sudo sed -i "s/#PermitRootLogin yes/PermitRootLogin no/" /etc/ssh/sshd_config
    echo -e "${GREEN}Вход под пользователем root отключен. Теперь root может отдохнуть!${NC}"
else
    echo -e "${YELLOW}Вход под пользователем root оставлен включенным. Это может быть менее безопасно. Как оставить ключи под ковриком!${NC}"
fi

# Дополнительная настройка для Ubuntu 22.04
if [[ "$VERSION_ID" == "22.04" ]]; then
    echo "PubkeyAcceptedAlgorithms +ssh-rsa" | sudo tee -a /etc/ssh/sshd_config
fi

# Установка прав на .ssh директорию и файл authorized_keys
chmod 700 ~/.ssh/
chmod 600 ~/.ssh/authorized_keys

# Перезапуск SSH сервера
echo -e "${BLUE}Перезапуск SSH сервера для применения изменений...${NC}"
sudo systemctl restart ssh

# Запрос на установку и настройку Fail2ban
read -p "Хотите установить и настроить Fail2ban для защиты от брутфорс-атак? (yes/no, по умолчанию yes): " install_fail2ban
install_fail2ban=${install_fail2ban:-yes}

if [ "$install_fail2ban" == "yes" ]; then
    echo -e "${BLUE}Установка Fail2ban...${NC}"
    sudo apt-get install fail2ban -y
    sudo systemctl enable fail2ban
    sudo systemctl start fail2ban
    echo -e "${GREEN}Fail2ban установлен и запущен. Теперь ваш сервер как крепость!${NC}"
else
    echo -e "${YELLOW}Установка Fail2ban пропущена. Надеюсь, у вас есть план Б!${NC}"
fi

# Перезапуск Fail2ban
echo -e "${BLUE}Перезапуск Fail2ban для применения настроек...${NC}"
sudo systemctl restart fail2ban

echo -e "${GREEN}Настройка завершена. Ваш сервер защищен. Как супергерой в мире серверов!${NC}"
sleep 3
# Юмор для криптоэнтузиастов
echo -e "${YELLOW}Помните, что в мире крипты, как и в жизни, главное — не потерять ключи!${NC}"
echo -e "${YELLOW}Если ваш сервер начнет майнить что то годное, не забудьте поделиться с нами частью прибыли)!${NC}"
echo
# Завершение
echo -e "${GREEN}С вами был PARADOX CLUB — место, где технологии встречаются с креативом!${NC}"

