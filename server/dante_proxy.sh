#!/bin/bash
    clear && echo -e "\e[35m
┌┬┐┌─┐┌┐┌┌┬┐┌─┐  ┌─┐┬─┐┌─┐─┐ ┬┬ ┬           
 ││├─┤│││ │ ├┤   ├─┘├┬┘│ │┌┴┬┘└┬┘           
─┴┘┴ ┴┘└┘ ┴ └─┘  ┴  ┴└─└─┘┴ └─ ┴ \e[0m" && echo
	mainmenu() {
		echo " 🛠️  | 1.Setup"
		echo " 🪬 | 2.Status"
		echo " 📚 | 3.Help"
		echo " 🗑️  | 4.Delete"
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
        echo -ne "\e[4;34mYour selection: \e[0m "
# Menu properties
	read -r ans
		case $ans in

			1) install ;;

			2) status ;;

			3) help	;;

			4) delet ;;

			0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh) ;;

			10)	echo $(printBCyan '"Bye bye."') && exit	;;

			*)	clear && echo -e "\e[33mInvalid request! \e[0m" && mainmenu ;;
	esac
}

# Functions
	delet(){
		systemctl stop danted.service
		sudo rm /etc/danted.conf
		sudo apt remove dante-server
		echo " \e[31mProxy deleted! \e[0m"
		mainmenu
		}

	status(){
		clear && printlogo && echo
		echo "\e[33mTo return, press Q \e[0m"
		echo
		systemctl status danted.service
		mainmenu
		}

	passwd(){
		echo -e "\e[33mEnter the username for connecting to the proxy \e[0m"
		echo -ne "\e[4;34mInput:\e[0m "
		read -r USERNAME
		sudo useradd -r -s /bin/false $USERNAME
		echo -e "\e[33mEnter the password for connecting to the proxy \e[0m"
		sudo passwd $USERNAME
		sudo systemctl restart danted
		}


	install(){
		echo -e "\e[33mSetting up... \e[0m"
		sudo apt update > /dev/null 2>&1
		sudo apt install dante-server > /dev/null 2>&1
		sudo rm /etc/danted.conf
		interface=$(ip -o -4 route show to default | awk '{print $5}')
		cat << EOF  > /etc/danted.conf
logoutput: syslog
user.privileged: root
user.unprivileged: nobody

# The listening network interface or address.
internal: 0.0.0.0 port=1080

# The proxying network interface or address.
external: $interface

# socks-rules determine what is proxied through the external interface.
socksmethod: username

# client-rules determine who can connect to the internal interface.
clientmethod: none

client pass {
    from: 0.0.0.0/0 to: 0.0.0.0/0
}

socks pass {
    from: 0.0.0.0/0 to: 0.0.0.0/0
}
EOF
		sudo ufw allow 1080
		echo -e "\e[33mEnter the username for connecting to the proxy \e[0m"
		echo -ne "\e[4;34mInput:\e[0m "
		read -r USERNAME
		sudo useradd -r -s /bin/false $USERNAME
		echo -e "\e[33mEnter the password for connecting to the proxy \e[0m"
		sudo passwd $USERNAME
		sudo systemctl enable danted.service
		sudo systemctl restart danted.service
		echo
		echo -e "\e[32mProxy started! \e[0m"
		mainmenu
		}

	help(){
		echo -e "\e[33m ============================================================= \e[0m"
		echo "  Dante - SOCKS5 proxy with open source code."
		echo "  SOCKS - less common protocol, but it is more effective"
		echo "  for some applications and is preferred for some types of traffic."
		echo "  It is used for HTTP for some types of traffic."
		echo -e "\e[33m  By default, the port is used \e[0m\e[32m1080 \e[0m"
		echo -e "\e[33m ============================================================= \e[0m"
		echo -ne "
To change the port, open the danted.conf file.
Execute the command: \e[32msudo nano /etc/danted.conf \e[0m
You will see the contents of the danted.conf file.
Find the line containing \e[35minternal: 0.0.0.0 port=1080 \e[0m
Usually, it is located on the 6th line of the file. Change the value of the port
to the desired one, for example: \e[35mport=8888. \e[0m

After making changes, press Ctrl + O, confirm saving the file, and then press Ctrl + X to exit the editor.

Restart the Dante service: \e[32msudo systemctl restart danted \e[0m

After restarting the Dante service, it will work on the new port,

Press Enter to exit the file, and then press Ctrl + X to exit the editor.

Restart the Dante service: \e[32msudo systemctl restart danted \e[0m

After restarting the Dante service, it will work on the new port,
specified in the configuration file.
\e[33m---------------------------------------------------------------- \e[0m"

mainmenu
}
mainmenu