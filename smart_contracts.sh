#!/bin/bash
    clear && echo -e "\e[35m
╔═╗╔╦╗╔═╗╦═╗╔╦╗  ╔═╗╔═╗╔╗╔╔╦╗╦═╗╔═╗╔═╗╔╦╗╔═╗
╚═╗║║║╠═╣╠╦╝ ║   ║  ║ ║║║║ ║ ╠╦╝╠═╣║   ║ ╚═╗
╚═╝╩ ╩╩ ╩╩╚═ ╩   ╚═╝╚═╝╝╚╝ ╩ ╩╚═╩ ╩╚═╝ ╩ ╚═╝\e[0m"
mainmenu() {
    echo
    echo "  ----------------"
    echo "  0.Back | 10.Exit"
    echo "  ----------------"
    echo -ne "\e[4;34mYour selection:\e[0m "
    read -r ans
    case $ans in
        0) source <(curl -s https://gitlab.com/blockchain-projekts/script/-/raw/main/launcher.sh) ;;
        10) echo "Goodbye." && exit ;;
        *) echo && echo -e "\e[31mInvalid request!\e[0m" && mainmenu ;;
    esac
}
mainmenu